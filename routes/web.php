<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('layout.master');
});

Route::get('/index', function () {
    return view('index');
});

Route::get('/bio', function () {
    return view('form');
});

Route::get('/data-table', function () {
    return view('data-table');
});

Route::get('/table', function () {
    return view('table');
});


// CRUD Cast
//Rouew tambah Cast
Route::get('/cast', [CastController::class, 'index']);
// Route::get('/cast', function () {
//     return 'berhasil';
// });

Route::get('/cast/create', [CastController::class, 'create']);

Route::post('/cast', [CastController::class, 'store']);

Route::get('/cast/{cast_id}', [CastController::class, 'show']);

Route::get('/cast/{cast_id}/edit', [CastController::class, 'edit']);

Route::put('/cast/{cast_id}/', [CastController::class, 'update']);
