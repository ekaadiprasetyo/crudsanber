@extends('layout.master')

@section('judul')
    <h1>Create Cast</h1>
@endsection

@section('content')    
    <form action="/cast" method="POST">
        @csrf
        <div class="form-group">
            <label>Nama</label>
            <input name="nama" type="text" value="{{old('nama')}}" class="form-control" placeholder="Nama">
        </div>
        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label>Umur</label>
            <input name="umur" type="text" value="{{old('umur')}}" class="form-control" placeholder="Umur">
        </div>
        @error('umur')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label>Bio</label>
            <textarea name="bio" class="form-control" value="{{old('bio')}}" cols="30" rows="10"></textarea>
        </div>
        @error('bio')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection