@extends('layout.master')

@section('judul')
    <h1>Halaman Tampil Cast</h1>
@endsection


@section('content')
<a href="/cast/create" class="btn btn-primary btn-sm my-3">Tambah</a>
<table class="table">
    <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Nama</th>
            <th scope="col">Action</th>
        </tr>
        </thead>
        <tbody>
            @forelse ($cast as $key => $item)
            <tr>
                <th scope="row">{{ $key + 1 }}</th>
                <td>{{ $item->nama }}</td>
                <td>
                    <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                    <a href="/cast/{{$item->id}}/edit" class="btn btn-warning  btn-sm">Edit</a>
                </td>
            </tr>
        
            @empty
                <h1>Data Kosong</h1>
            @endforelse
        </tbody>
</table>
@endsection